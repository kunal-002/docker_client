import os

from loguru import logger
import requests


class DockerServices:
    def __init__(self):
        self.token = ""
        self.token_type = ""
        pass

    # def starting(self, username, password):
    #     # Making a get request
    #     response = requests.post('http://0.0.0.0:5000/token', data={"username": username, "password": password})
    #
    #     # print request object
    #     print(response.json())
    #     res = response.json()
    #     self.token = res["access_token"]
    #     self.token_type = res["token_type"]
    #     # token = res["access_token"]
    #     # print(token)
    #     # return token
    #     return res
    #
    # def get_service_by_service_id(self, service_id):  # service_token
    #     try:
    #         # self.service_token = service_token
    #         """
    #             Query the Database and Check if Service Id exists
    #
    #             """
    #         # 1)  Get request to list Services
    #         r_serv = requests.get(f"http://0.0.0.0:5000/services/list/{service_id}")
    #         # print(r_serv.json())
    #         r_serv_json = r_serv.json()
    #         if "Service does not exists.." in r_serv_json.values():
    #             raise Exception("Service does not exists..")
    #         if not r_serv_json["data"][0]["is_deleted"]:
    #             # service_name = r_serv_json["data"][0]["service_name"]
    #             # service_friendly_name = r_serv_json["data"][0]["service_friendly_name"]
    #             # version = r_serv_json["data"][0]["version"]
    #             # s_type_id = r_serv_json["data"][0]["s_type_id"]
    #             dict_service = {"Service Name": r_serv_json["data"][0]["service_name"],
    #                             "Service Friendly name": r_serv_json["data"][0]["service_friendly_name"],
    #                             "Version": r_serv_json["data"][0]["version"],
    #                             "S Type id": r_serv_json["data"][0]["s_type_id"]}
    #             # dict_service = {"Service Name":service_name,"Service Friendly name":service_friendly_name, "Version":version, "S Type Id":s_type_id}
    #             # logger.info(f"Service Name:-{service_name}, Service Friendly name :- {service_friendly_name}, Version:- {version}, s_type_id:-{s_type_id}")
    #             # print(dict_service)
    #             logger.info(f"get_service_by_service_id() executed :- {dict_service} is returned..")
    #             return dict_service
    #         else:
    #             raise Exception("Service is Deleted !!")
    #
    #     except Exception as e:
    #         logger.info(f'Error in get_service_by_service_id()---> {e}')
    #
    # def get_all_services(self):
    #     try:
    #         serv = requests.get(f"http://0.0.0.0:5000/service/list_all")
    #         r_serv_json = serv.json()
    #         # print(r_serv_json)
    #         logger.info(f"get_all_services() executed {r_serv_json} returned..")
    #         return r_serv_json
    #     except Exception as e:
    #         logger.info(f'Error in get_all_services()---> {e}')
    #
    # def get_credentials_by_credentials_id(self, credentials_id):
    #     try:
    #         # self.service_token = service_token
    #
    #         # 1)  Get request to list credentials
    #         r_cred = requests.get(f"http://0.0.0.0:5000/credentials/list/{credentials_id}")
    #         # print(r_serv.json())
    #         r_cred_json = r_cred.json()
    #         if "Credentials does not exists.." in r_cred_json.values():
    #             raise Exception("Credentials does not exists..")
    #         if not r_cred_json["data"][0]["is_deleted"]:
    #             dict_cred = {"Credentials": r_cred_json["data"][0]["credentials"],
    #                          "Credentials Name": r_cred_json["data"][0]["credentials_name"],
    #                          "Resources id": r_cred_json["data"][0]["resources_id"]
    #                          }
    #             # dict_service = {"Service Name":service_name,"Service Friendly name":service_friendly_name, "Version":version, "S Type Id":s_type_id}
    #             # logger.info(f"Service Name:-{service_name}, Service Friendly name :- {service_friendly_name}, Version:- {version}, s_type_id:-{s_type_id}")
    #             # print(dict_cred)
    #             logger.info(f"get_credentials_by_credentials_id() executed :- {dict_cred} is returned..")
    #             return dict_cred
    #         else:
    #             raise Exception("Credential is Deleted !!")
    #
    #     except Exception as e:
    #         logger.info(f'Error in get_credentials_by_credentials_id()---> {e}')
    #
    # def get_configs_by_config_id(self, configs_id):
    #     try:
    #         # self.service_token = service_token
    #
    #         # 1)  Get request to list credentials
    #         conf = requests.get(f"http://0.0.0.0:5000/configs/list/{configs_id}")
    #         # print(r_serv.json())
    #         conf_json = conf.json()
    #         if "Config does not exist.." in conf_json.values():
    #             raise Exception("Config does not exist..")
    #         if not conf_json["data"][0]["is_deleted"]:
    #             dict_conf = {"Config": conf_json["data"][0]["configs"],
    #                          "Config Name": conf_json["data"][0]["configs_name"]
    #                          }
    #             # dict_service = {"Service Name":service_name,"Service Friendly name":service_friendly_name, "Version":version, "S Type Id":s_type_id}
    #             # logger.info(f"Service Name:-{service_name}, Service Friendly name :- {service_friendly_name}, Version:- {version}, s_type_id:-{s_type_id}")
    #             # print(dict_conf)
    #             logger.info(f"get_configs_by_config_id() executed :- {dict_conf} is returned..")
    #             return dict_conf
    #         else:
    #             raise Exception("Config is Deleted !!")
    #
    #     except Exception as e:
    #         logger.info(f'Error in get_configs_by_config_id()---> {e}')
    #
    # def get_mapping_by_service_id(self, service_id):
    #     try:
    #         # self.service_token = service_token
    #
    #         # 1)  Get request to list credentials
    #         mapp = requests.get(f"http://0.0.0.0:5000/mappings/list/by_service_id/{service_id}")
    #         # print(r_serv.json())
    #         mapp_json = mapp.json()
    #         if "Mapping does not exists.." in mapp_json.values():
    #             raise Exception("Mapping does not exists..")
    #         if not mapp_json["data"][0]["is_deleted"]:
    #             mapp_conf = {"Service id": mapp_json["data"][0]["service_id"],
    #                          "credential id": mapp_json["data"][0]["credentials_id"],
    #                          "config id": mapp_json["data"][0]["configs_id"]
    #                          }
    #             # dict_service = {"Service Name":service_name,"Service Friendly name":service_friendly_name, "Version":version, "S Type Id":s_type_id}
    #             # logger.info(f"Service Name:-{service_name}, Service Friendly name :- {service_friendly_name}, Version:- {version}, s_type_id:-{s_type_id}")
    #             # print(mapp_conf)
    #             logger.info(f"get_mapping_by_service_id() executed :- {mapp_conf} is returned..")
    #             return mapp_conf
    #         else:
    #             raise Exception("Mapping is Deleted !!")
    #
    #     except Exception as e:
    #         logger.info(f'Error in get_mapping_by_service_id()---> {e}')
    #
    # def get_service_by_service_name(self, service_name):  # service_token
    #     try:
    #         # self.service_token = service_token
    #         """
    #             Query the Database and Check if Service Id exists
    #
    #             """
    #         # 1)  Get request to list Services
    #         r_serv = requests.get(f"http://0.0.0.0:5000/services/list_name/{service_name}")
    #         # print(r_serv.json())
    #         r_serv_json = r_serv.json()
    #         if "Service does not exists.." in r_serv_json.values():
    #             raise Exception("Service does not exists..")
    #         if not r_serv_json["data"][0]["is_deleted"]:
    #             # service_name = r_serv_json["data"][0]["service_name"]
    #             # service_friendly_name = r_serv_json["data"][0]["service_friendly_name"]
    #             # version = r_serv_json["data"][0]["version"]
    #             # s_type_id = r_serv_json["data"][0]["s_type_id"]
    #             dict_service = {"Service Name": r_serv_json["data"][0]["service_name"],
    #                             "Service Friendly name": r_serv_json["data"][0]["service_friendly_name"],
    #                             "Version": r_serv_json["data"][0]["version"],
    #                             "S Type id": r_serv_json["data"][0]["s_type_id"]}
    #             # dict_service = {"Service Name":service_name,"Service Friendly name":service_friendly_name, "Version":version, "S Type Id":s_type_id}
    #             # logger.info(f"Service Name:-{service_name}, Service Friendly name :- {service_friendly_name}, Version:- {version}, s_type_id:-{s_type_id}")
    #             # print(dict_service)
    #             logger.info(f"get_service_by_service_name() executed :- {dict_service} is returned..")
    #             return dict_service
    #         else:
    #             raise Exception("Service is Deleted !!")
    #
    #     except Exception as e:
    #         logger.info(f'Error in get_service_by_service_id()---> {e}')

    def get_info(self, service_name, username, password):
        try:
            # headers = {"token_type":}
            host = os.getenv("HOST")
            port = os.getenv("PORT")
            serv_all = requests.post(f"{host}:{port}/mappings/list_complete/{service_name}",
                                     data={"username": username, "password": password})
            serv_all_json = serv_all.json()
            if "Not Found" in serv_all_json.values():
                raise Exception("Not Found..")
            if "Incorrect username or password" in serv_all_json.values():
                raise Exception("Incorrect username or password..")
            if "User is deactivated.." in serv_all_json.values():
                raise Exception("User is deactivated..")
            if "You are not authorized..sorry.." in serv_all_json.values():
                raise Exception("You are not authorized..sorry..")
            if "Service is deleted.." in serv_all_json.values():
                raise Exception("Service is deleted..")
            if "Service does not exists.." in serv_all_json.values():
                raise Exception("Service does not exists..")
            if "Credentials_id does not exists.." in serv_all_json.values():
                raise Exception("Credentials_id does not exists..")
            if "Credentials_id does not exists.." in serv_all_json.values():
                raise Exception("Configs_id does not exists..")
            if "Credentials does not exists.." in serv_all_json.values():
                raise Exception("Credentials does not exists..")
            if "Configs does not exists.." in serv_all_json.values():
                raise Exception("Configs does not exists..")
            if "Mapping Does Not exist.." in serv_all_json.values():
                raise Exception("Configs and Credentials mapping does not exists for this service..")
            if serv_all_json["credentials"][0][4] is True:
                raise Exception("Credentials are Deleted !!")
            if serv_all_json["configs"][0][4] is True:
                raise Exception("Configs are Deleted !!")

            else:
                cred_dict = {"Credentials ": serv_all_json["credentials"][0][1],
                             "Credentials Name ": serv_all_json["credentials"][0][2]}
                conf_dict = {"Configs ": serv_all_json["configs"][0][1],
                             "Configs Name ": serv_all_json["configs"][0][2]}
                logger.info(f"get_service_by_service_name() executed {cred_dict, conf_dict} are returned..")
                return cred_dict, conf_dict

        except Exception as e:
            logger.info(f'Error in get_all_by_service_name()---> {e}')
